
/*
,, run entire file up to first \q.
,sa creates select * query for the table name under cursor
,ss creates select field list from psql result

nmap <buffer>,, :w<CR>:silent !psql -f "%" >  /tmp/err 2>&1 <CR>:e ++enc=utf-8 /tmp/err<CR>:se ft=xml <CR>zR
nmap <buffer>,sa "xyiwo-- <C-R>x --{{{<CR>select *<CR>from <C-R>x<CR>limit 1<CR>;<CR>\q<CR>-- }}}<Esc>%j_
nmap <buffer>,ss :s/^\s*/  /<CR>:s/\s*$//<CR>:s/ *\| */\r ,/g<CR>
*/
