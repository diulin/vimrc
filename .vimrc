" ----------------------------------------------------------------------------------------------------
" @file         .vimrc
" @description  Vim configuration file
" @author       Andriy Diulin (diulin@gmail.com)
" vim: fdm=marker noet ts=4 sts=4 sw=4
" ----------------------------------------------------------------------------------------------------


set rtp+=$HOME/.vim
autocmd!
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
filetype plugin indent on
syntax on
"
""Basic option"{{{
set et
set virtualedit=block
set nowrap
set noincsearch
set whichwrap-=h,l
set listchars=tab:\|_,trail:�,extends:>,precedes:<,nbsp:+
set list listchars=tab:\|_,trail:�
filetype plugin on
runtime macros/matchit.vim
set nocompatible
set visualbell
set nospell
set nonumber
syntax on
set noerrorbells
set guioptions="ra" "for *really* hard ones: no menu, no toolbar...
set diffopt+=vertical,filler,foldcolumn:3
set autowriteall
set autoread
set infercase
set wildmenu
set ruler
set autoindent
set modeline
set lazyredraw
set ffs=unix,dos,mac
set tabstop=2
set shiftwidth=2
set softtabstop=2
set formatoptions=qn1
set textwidth=100
set scrolloff=3
set sidescrolloff=3
set cursorline
set backupdir=~/vim~
set directory=~/vim~
set history=2000
set path=.,,**
set shortmess=at " all messages abbreviated
set cmdheight=1
set keywordprg=:help
set synmaxcol=250 " yes vim is not genius when dealing with long lines

""Show (partial) command in the last line of the screen.
""In Visual mode the size of the selected area is shown:
set showcmd
set wildmode=longest:full
set hidden
set mouse=
set mousehide
"
"

" Leader key timeout
set tm=2000
set ignorecase
set wildignorecase
set showmatch
set showcmd  "shows partial command and size of selected area in Visual mode
set hlsearch
"
set scrolloff=3
set sidescroll=1
set sidescrolloff=10
set virtualedit+=block
"...Oh, and man' never ever let Vim write a backup file! They did that in the 70's. Use modern ways for tracking your changes, for God's sake.
set nobackup
set noswapfile
set ttyfast
" Set to auto read when a file is changed from the outside
set autoread



" love with system clipboard"{{{
if has('clipboard')
  if has('unnamedplus')  " When possible use + register for copy-paste
  set clipboard=unnamedplus,unnamed
  else         " On mac and Windows, use * register for copy-paste
  set clipboard=unnamed
  endif
endif
"}}}
" Highlight parenthesis."{{{
set showmatch
" Highlight when CursorMoved.
set cpoptions-=m
set matchtime=1
" Highlight <>.
set matchpairs+=<:>
"}}}
" undo option {{{
set undofile
set undodir=$HOME/vim~/undo
set undolevels =1000 "maximum number of changes that can be undone
set undoreload =10000 "maximum number lines to save for undo on a buffer reload
au BufWritePre /tmp/* setlocal noundofile
"}}}

"}}}

"Cyrillic letters"{{{

""ukrainian letters"{{{
function! ToggleUkr()
  if (exists("b:ukr")==0)
  let b:ukr=0
  endif
  if (b:ukr==0)
  exe 'setlocal keymap=ukrainian-jcuken'
  let b:ukr=1
  else
  exe 'setlocal keymap=russian-jcukenwin'
  let b:ukr=0
  endif
endfunction
"
command! ToggleUkr :call ToggleUkr()
"
"}}}
set keymap=ukrainian-jcuken
set iminsert=0
set imsearch=0
set iskeyword=@,48-57,_,128-167,?-?
"" cyrillic letters in filenames
set isf+=224-255


"changing keymap
imap ,, 
cmap ,, 

menu Encoding.CP1251   :e ++enc=cp1251<CR>
menu Encoding.CP866    :e ++enc=cp866<CR>
menu Encoding.KOI8-R   :e ++enc=koi8-r<CR>
menu Encoding.UTF-8    :e ++enc=utf-8<CR>
menu Encoding.latin1   :e ++enc=latin1<CR>
map ,fe :emenu Encoding.

""}}}

"nice autocmd"{{{
"sudo write
"command W w !sudo tee % > /dev/null
au FileType vbnet set foldmethod=syntax
au FileType vbnet set commentstring='%s
let g:xml_syntax_folding=1
au FileType {xml,xsd,xslt} setlocal foldmethod=syntax
au FileType {xml,xsd,xslt} setlocal foldlevel=1
au FileType {xml,xsd,xslt} vmap ,px !xmllint --format  --encode utf-8 - <CR>

"au FileType sql setlocal equalprg=/home/and/bin/fsqlf\ --keyword-case\ lower\ --config-file\ ~/.fsqlfrc

au FileType javascript,json call JavaScriptFold()
au FileType javascript,json set foldmethod=syntax
function! JavaScriptFold() 
  setl foldmethod=syntax
  setl foldlevelstart=1
  syn region foldBraces start=/{/ end=/}/ transparent fold keepend extend

  "function! FoldText()
  "    return substitute(getline(v:foldstart), '{.*', '{...}', '')
  "endfunction
  "setl foldtext=FoldText()
endfunction


"set foldlevelstart=0
"set foldmethod=syntax

augroup vimrcFold
  " fold vimrc itself by categories
  autocmd!
  autocmd FileType vim set foldmethod=marker
  autocmd FileType vim set foldlevel=0
augroup END



" Make directory automatically."{{{
" --------------------------------------
" http://vim-users.jp/2011/02/hack202/
augroup MyAutoCmd
  autocmd MyAutoCmd BufWritePre * call s:mkdir_as_necessary(expand('<afile>:p:h'), v:cmdbang)
  function! s:mkdir_as_necessary(dir, force)
  if !isdirectory(a:dir) && &l:buftype == '' &&
  	\ (a:force || input(printf('"%s" does not exist. Create? [y/N]',
  	\              a:dir)) =~? '^y\%[es]$')
  call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
  endif
  endfunction
augroup END
"}}}
" make change to insert mode more visible"{{{
":autocmd InsertEnter * set cul
":autocmd InsertLeave * set nocul
"}}}
"}}}

"mappings "{{{
menu Encoding.WIN      :e ++enc=cp1251<CR>
menu Encoding.DOS      :e ++enc=cp866<CR>
menu Encoding.KOI8-R   :e ++enc=koi8-r<CR>
menu Encoding.UTF-8    :e ++enc=utf-8<CR>
menu Encoding.latin1   :e ++enc=latin1<CR>
map ,fe :emenu Encoding.

"unmap <Leader>f
"unmap <Leader>F
"unmap <Leader><space>

nmap <silent> <leader>fn <ESC>:call ToggleFindNerd()<CR>

"back to normal mode
let mapleader = ","
imap jj <Esc>

"imap <F2> <Esc>a%<C-R>=strftime("%Y-%m-%d %H:%M:%S")<CR>
"nmap <F2> a%<C-R>=strftime("%Y-%m-%d %H:%M:%S")<CR><Esc>

" matchit jumps wit Tab"{{{
nmap <Tab> %
vmap <Tab> %

noremap H ^
noremap L $
map j gj
map k gk

"change buffer
nnoremap <C-H> :bp<CR>g`"
nnoremap <C-L> :bn<CR>g`"

" emacs-like bindings for insert mode
imap <C-A> <Home>
""go end
imap <C-E> <End>
""char left
imap <C-B> <Left>
""char right
imap <C-F> <Right>
""delete
imap <C-D> <Del>
cmap <C-D> <Del>

"}}}
"rise main file (assuming there is only one opened in session)"{{{
map <Leader>bb :b main<CR>
"}}}
"yankig"{{{
"" Y works like D, yanks to end of line
map Y y$
""" copy selected into the "+" buffer
vmap <Leader>yy "+y
""" copy selected into the "+" buffer
nmap <Leader>pp "+p
""" copy selected into the "+" buffer
vmap <Leader>pp "+p
"" copy whole buffer into clipboard
nmap <Leader>ya ggVG"+y2<C-o>
"" replace whole buffer with clipboard content
nmap <Leader>pa ggVG"+p
"" select whole buffer
nmap <Leader>va ggVG
"}}}

"insert <CR>
nnoremap <C-J> i<C-J><Esc>

"copy entire buffer
nnoremap <Leader>fy :1,$y<CR>

"cd to current file dir
nnoremap <Leader>fd :cd %:p:h<CR>:pwd<CR>
"Copy the full path of the current file to the clipboard
nnoremap <silent> <Leader>fp :let @+=expand("%:p")<cr>:echo "Copied current file path '".expand("%:p")."' to clipboard"<cr>

" simple calculator
nmap <Leader>ac :call append(line("."),  printf("%f",eval(getline(line(".")))))<CR>

"search selected
vmap <silent> //  y/<C-R>=substitute(escape(@", '\\/.*$^~[]'), "\n", "\\\\n", "g")<CR><CR>
" search in helpfiles"{{{
"nmap <Leader>K "hyiw:he index\| :tse  /<C-R>h<CR>
"nmap <Leader>k "hyiw:he index\| :tse  <C-R>h<CR>
"vmap <Leader>K "hy:he index\| :tse  /<C-R>h<CR>
"vmap <Leader>k "hy:he index\| :tse  <C-R>h<CR>

"!plugin! man pages
nmap <Leader>mk "tyiw:Man <C-R>t<CR>
vmap <Leader>mk "ty:Man <C-R>t<CR>
"}}}

" split settings {{{
set splitbelow
set splitright

"splits and windows
nnoremap <Leader>wj <C-W><C-J>
nnoremap <Leader>wk <C-W><C-K>
nnoremap <Leader>wl <C-W><C-L>
nnoremap <Leader>wh <C-W><C-H>
nnoremap <Leader>wo :copen<CR>
"}}}

"folding"{{{
nmap zz za
nmap <Leader>zn 2o<Esc>Vkzfza

nmap <Leader>zo O<Esc>jo<Esc>V2kzfza
vmap <Leader>zo "zd2o<Esc>Vkzf"zpza

"}}}


"}}}
"
"functions"{{{
func! ChangeQuotes()
  silent! s/'/#####/g
  silent! s/"/'/g
  silent! s/#####/"/g
endfunc
"}}}

"look "{{{
set background=dark
colorscheme solarized
set gfn=DejaVu_Sans_Mono_for_Powerline:h12:cRUSSIAN:qDRAFT

"set gfn=DejaVu\ Sans\ Mono\ for\ Powerline\ 12
"to show statusline
set laststatus=2
"Statusline "{{{

set statusline=
set statusline+=%#PmenuSel#
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 
"}}}
"}}}

""Plugin settings "{{{
"EightHeaderFolds"{{{
let &foldtext = "EightHeaderFolds( '\\=s:fullwidth-2', 'left', [ repeat( '  ', v:foldlevel - 1 ), '.', '' ], '\\= s:foldlines . \" lines\"', '' )"
"}}}

""}}}




