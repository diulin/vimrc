
augroup filetypedetect
  au! BufRead,BufNewFile main set filetype=main
augroup END

augroup filetypedetect
  au! BufRead,BufNewFile line set filetype=timeline
augroup END


augroup filetypedetect
  au! BufRead,BufNewFile *.1[sc]	setfiletype 1c
  au! BufRead,BufNewFile *.vba,*.vb,*.vbs,*.bas,*.cls set filetype=vbnet
  au! BufRead,BufNewFile *.t2t	setfiletype txt2tags
  au! BufRead,BufNewFile *.cos setfiletype cos
  au! BufRead,BufNewFile *.arc setfiletype scheme
  au! BufRead,BufNewFile *.asd setfiletype lisp
augroup END

