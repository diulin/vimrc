nmap zz za

menu Encoding.CP1251   :e ++enc=cp1251<CR>
menu Encoding.CP866    :e ++enc=cp866<CR>
menu Encoding.KOI8-R   :e ++enc=koi8-r<CR>
menu Encoding.UTF-8    :e ++enc=utf-8<CR>
menu Encoding.latin1   :e ++enc=latin1<CR>
map ,fe :emenu Encoding.

"" Y works like D, yanks to end of line
map Y y$
""" copy selected into the "+" buffer
vmap ,yy "+y
""" copy selected into the "+" buffer
nmap ,pp "+p
""" copy selected into the "+" buffer
vmap ,pp "+p
"" copy whole buffer into clipboard
nmap ,ya ggVG"+y2<C-o>
"" replace whole buffer with clipboard content
nmap ,pa ggVG"+p
"" select whole buffer
nmap ,va ggVG

noremap <leader><space> :noh<cr>:call clearmatches()<cr>

runtime macros/matchit.vim
map <tab> %

" Easier to type, and I never use the default behavior.
noremap H ^
noremap L $

" Open a Quickfix window for the last search.
nnoremap <silent> <leader>/ :execute 'vimgrep /'.@/.'/g %'<CR>:copen<CR>


"nmap <silent> ,/ :let @/=""<CR>
"vmap <silent> //  "sy/<C-R>=substitute(escape(@s, '\\/.*$^~[]'), "\n", "\\\\n", "g")<CR><CR>
vmap <silent> // "sy/<C-R>=substitute(escape(@s, '\\/.*$^~[]'), "\n", "\\\\n", "g")<CR><CR>
"
"
"" Very handy (for myself) mappings
nmap ,fd :cd %:p:h<CR>:pwd<CR>
nmap <C-J> i<C-J><Esc>
nnoremap <C-H> :bp<CR>g`"
nnoremap <C-L> :bn<CR>g`"

map j gj
map k gk
"" <S-Y> works similar to <S-D>
nmap <S-Y> y$
"easy change keymap 
imap ,, 
cmap ,, 
"easy back to normal mode
imap jj <Esc>

" emacs-like bindings for inser mode
imap <C-A> <Home>
""go end
imap <C-E> <End>
""char left
imap <C-B> <Left>
""char right
imap <C-F> <Right>
""up
imap <M-C-K> <Up>
""down
imap <M-C-J> <Down>
""delete
imap <C-D> <Del>


cmap <C-D> <Del>

nmap <F2> <Esc>a<C-R>=strftime("%Y.%m.%d-%H:%M:%S")<CR> 
imap <F2> <Esc>a<C-R>=strftime("%Y.%m.%d-%H:%M:%S")<CR> 
imap <F3> <Esc>a<C-R>=strftime("%R")<CR> 
cmap <F2> <C-R>=strftime("%Y%m%d-%H%M%S")<CR> 


"" simple calculator
nmap ,ac :call append(line("."),  printf("%f",eval(getline(line(".")))))<CR>


"" search in helpfiles
nmap ,K "hyiw:he index\| :tse  /<C-R>h<CR>
nmap ,k "hyiw:he index\| :tse  <C-R>h<CR>
vmap ,K "hy:he index\| :tse  /<C-R>h<CR>
vmap ,k "hy:he index\| :tse  <C-R>h<CR>

"aligning tables
map ,tt mq{jV}k:Align \|<CR>'q
map ,t; mq{jV}k:Align \;<CR>'q


"rise main file (assuming there is only one opened in session)"{{{
map <Leader>bb :b main<CR>
