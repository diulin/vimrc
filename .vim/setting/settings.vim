" ----------------------------------------------------------------------------------------------------

" @file         settings.vim
" @description  Global settings
" @author       Андрій Діулін (diulin@gmail.com)
" ----------------------------------------------------------------------------------------------------
"Basic option"{{{
set nocompatible
set encoding=utf-8
lan messages 'en_US.UTF-8'
set visualbell
set noerrorbells
set guioptions="ra" "for *really* hard ones: no menu, no toolbar...
set diffopt+=vertical,filler,foldcolumn:3
set nowrap
set autowriteall
set autoread
set infercase
set wildmenu
set ruler
set autoindent
set modeline
set lz
set ffs=unix,dos,mac
set tabstop=2
set shiftwidth=2
set softtabstop=2
set formatoptions=qrn1
set textwidth=80
set scrolloff=3
set sidescrolloff=3
set cursorline
set backupdir=~/vim~
set directory=~/vim~
set history=2000


set keywordprg=:help


""Show (partial) command in the last line of the screen.
""In Visual mode the size of the selected area is shown:
set showcmd
set wildmode=longest:full
set hidden
set mouse=
set mousehide


set ignorecase
"set smartcase
"set incsearch
set showmatch
set hlsearch

set scrolloff=3
set sidescroll=1
set sidescrolloff=10
set virtualedit+=block
""...Oh, and man? never ever let Vim write a backup file! They did that in the 70?s. Use modern ways for tracking your changes, for God?s sake."{{{
set nobackup
set noswapfile
set ttyfast
"}}}
"}}}
"Cyrillic letters"{{{
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
set iskeyword=@,48-57,_,128-167,?-?
"" cyrillic letters in filenames
set isf+=224-255
""}}}
" StatusLine"{{{
augroup ft_statuslinecolor
    au!

    au InsertEnter * hi StatusLine ctermfg=196 guifg=#FF3145
    au InsertLeave * hi StatusLine ctermfg=130 guifg=#CD5907
augroup END

set statusline=%{(&iminsert)?'*':'#'}    " Current keymap
"set statusline+=%{fugitive#statusline()}
set statusline+=[%n] " Buffer #.
set statusline+=%f   " Path.
set statusline+=%m   " Modified flag.
set statusline+=%r   " Readonly flag.
set statusline+=%w   " Preview window flag.

set statusline+=\    " Space.

set statusline+=%=   " Right align.
" File format, encoding and type.  Ex: "(unix/utf-8/python)"
set statusline+=
set statusline+=(
set statusline+=%{&ff}                        " Format (unix/DOS).
set statusline+=/
set statusline+=%{strlen(&fenc)?&fenc:&enc}   " Encoding (utf-8).
set statusline+=/
set statusline+=%{&ft}                        " Type (python).
set statusline+=)

" Line and column position and counts.
set statusline+=\ (line\ %l\/%L,\ col\ %03v,\ char\ %04b)
""	The value of this option influences when the last window will have a
""	status line:
set laststatus=2
"}}}
" undo option {{{
set undofile
set undodir=$HOME/vim~/undo
set undolevels =1000 "maximum number of changes that can be undone
set undoreload =10000 "maximum number lines to save for undo on a buffer reload
au BufWritePre /tmp/* setlocal noundofile
"}}}
" Colorscheme {{{
set background=dark
colorscheme solarized
"set gfn=Lucida_Console:h12:cRUSSIAN:qDRAFT
set gfn=DejaVu_Sans_Mono_for_Powerline:h12:cRUSSIAN:qDRAFT
"}}}
" Leaders {{{
let mapleader = ","
let maplocalleader = ","
"}}}
" Syntax {{{
" Show the stack of syntax hilighting classes affecting whatever is under the  
" cursor.
syntax on
let g:xml_syntax_folding = 1
function! SynStack()
  echo join(map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")'), " > ")
endfunc 
" }}}
" Various filetype-specific stuff --------------------------------------------- {{{
" Javascript {{{
augroup ft_javascript
    au!
		au FileType javascript setlocal foldmethod=marker
		au FileType javascript setlocal foldmarker={,}
		"au FileType javascript nmap <F3> :%s/{/{\r/g<CR>:%s/}/\r}/g<CR>:%s/,/,\r/g<CR>:set ft=javascript<CR>ggVG=
		"au FileType javascript vmap ,= :s/{/{\r/g<CR>:s/}/\r}/g<CR>:s/,/,\r/g<CR>:set ft=javascript<CR>ggVG=
augroup END
" }}}
" XML {{{
augroup ft_xml
    au!
    au FileType xml setlocal foldmethod=syntax
		au FileType xml map <F3> :%s/>\s*</>\r</g<CR>:set ft=xml<CR>ggVG=
augroup END
" }}}
" Vim {{{
augroup ft_vim
    au!
    au FileType vim setlocal foldmethod=marker
    au FileType help setlocal textwidth=78
    "au BufWinEnter *.txt if &ft == 'help' | wincmd L | endif
augroup END

" }}}
" Lisp {{{

augroup ft_lisp
    au!
    au FileType lisp call TurnOnLispFolding()
augroup END

" }}}
" }}}
" love with system clipboard"{{{
if has('clipboard')
	if has('unnamedplus')  " When possible use + register for copy-paste
		set clipboard=unnamedplus,unnamed
	else         " On mac and Windows, use * register for copy-paste
		set clipboard=unnamed
	endif
endif
"}}}
