" Fuzzy file search"{{{
let g:loaded_ctrlp = 1
let g:ctrlp_map = ',ff'
let g:ctrlp_regexp_search = 1
let g:ctrlp_persistent_input = 0
let g:ctrlp_ignore_space = 1 
let g:ctrlp_working_path_mode = 1 "the parent directory of the current file.
"}}}
"" NerdTree
nmap ,n :NERDTreeClose<CR>:NERDTreeToggle<CR>
nmap ,m :NERDTreeClose<CR>:NERDTreeFind<CR>
nmap ,N :NERDTreeClose<CR>
"" Store the bookmarks file
let NERDTreeBookmarksFile=expand("$HOME/.vim/NERDTreeBookmarks")
"" Don't display these kinds of files
let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.py\$class$', '\.obj$','\.o$', '\.so$', '\.egg$', '^\.git$' ]
let NERDTreeShowBookmarks=1       " Show the bookmarks table on startup
let NERDTreeShowFiles=1           " Show hidden files, too
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1          " Quit on opening files from the tree
let NERDTreeHighlightCursorline=1 " Highlight the selected entry in the tree
let NERDTreeMouseMode=2           " Use a single click to fold/unfold directories

"for Align plugin
let g:Align_xstrlen = 1
