
if !exists("*A2N")
"  finish
function! A2N(aaa)
  let ans=0
  let sss=a:aaa
  let lll=strlen(sss)
  let ttt=lll-1
  let md=0
  let mmm=1
  while ttt >= 0 
    let md = char2nr(strpart(sss,ttt,1))
    if md > 57
      let ans = ans + (md-55) * mmm
    else  
      let ans = ans + (md-48) * mmm
    endif
    let mmm = 36 * mmm
    let ttt = ttt-1
  endwhile
  return ans
endfunction
endif 

if !exists("*N2A")
"  finish
function! N2A(nnn)
  let ans=""
  let char=""
  let nr=a:nnn
  let md=0
  while nr > 0
    let md = nr%36
    let char = (md > 9 ? nr2char(md+55) :nr2char(md+48))
    let ans=char . ans
    let nr = nr/36
  endwhile
  return ans
endfunction
endif 
