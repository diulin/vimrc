let vimclojure#HighlightBuiltins=1 
let vimclojure#ParenRainbow=1

let tlist_clojure_settings = 'lisp;f:function'
let Tlist_Exit_OnlyWindow=1

let vimclojure#WantNailgun=1
let vimclojure#NailgunClient="/home/and/pp/clojure/vimclojure-nailgun-client/ng"
