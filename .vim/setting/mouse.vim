:noremap <ScrollWheelUp>	    <nop>
:noremap <S-ScrollWheelUp>	  <nop>
:noremap <C-ScrollWheelUp>	  <nop>
:noremap <ScrollWheelDown>	  <nop>
:noremap <S-ScrollWheelDown>	<nop>
:noremap <C-ScrollWheelDown>	<nop>
:noremap <MiddleMouse> <nop>
:noremap! <MiddleMouse> <nop> 
:noremap <LeftMouse> <nop>
:noremap! <LeftMouse> <nop> 
:noremap <RightMouse> <nop>
:noremap! <RightMouse> <nop> 
:noremap <2-LeftMouse> <nop> 
:noremap! <2-LeftMouse> <nop> 

