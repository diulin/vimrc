" Vim filetype plugin file
" Maintainer:	Diulin Andriy 
" Last Change:	2016-08-01


" Only do this when not done yet for this buffer
"if exists("b:did_ftplugin")
"  finish
"endif

let b:did_ftplugin = 1

let maplocalleader = ","
"
" exec vim command (whole line under cursor)
imap <buffer> <F2> <Esc>#o<C-R>=strftime("%Y-%m-%d %H:%M:%S")<CR> {{{<CR>}}}<CR><Esc>O

nmap <buffer> <F2> o#<C-R>=strftime("%Y-%m-%d %H:%M:%S")<CR> {{{<CR>}}}<Esc>O

setlocal commentstring=#\ %s
setlocal ts=2


setlocal foldmethod=marker
setlocal foldmarker={{{,}}}

