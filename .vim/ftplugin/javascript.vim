" Vim filetype plugin file
" Language:	Cache Object Script (terminal)	
" Maintainer:	Diulin Andriy 
" Last Change:	2009 Sep 20


" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")

	if !hasmapto('<Plug>jsMessageExpression')
		nmap <buffer><LocalLeader>me <Plug>jsMessageExpression
	endif
	nnoremap <buffer><Plug>jsMessageExpression "xyiw:let b:tmp="log(\"(debug " . tolower(N2A(strftime("%Y%m%d%H%M%S"))) . " on " . line(".") . ") ". @x . " = \" + ".@x.");"<CR>o<C-R>=b:tmp<CR><Esc>
	nnoremap ,me 
endif
