" Vim filetype plugin file
" Language:	Cache Object Script (terminal)	
" Maintainer:	Diulin Andriy 
" Last Change:	2009 Sep 20


" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif


let b:did_ftplugin = 1

let maplocalleader = ","
"
"" exec vim command (whole line under cursor)
"nmap <buffer>,. :<C-R>=getline(".")<CR><CR>
"nmap <buffer>,,  :call system('<C-R>=getline(".")<CR> > /tmp/err.err 2>&1')<CR>:e /tmp/err.err<CR>

"vmap <buffer>,. <Esc>:call ExecSelected(0)<CR>
"vmap <buffer>,, <Esc>:call ExecSelected(1)<CR>

setlocal commentstring=;\ %s

setlocal ts=2


setlocal foldmethod=marker
setlocal foldmarker={{{,}}}


if !exists("*AuthorChange")
  function! AuthorChange()
		let b:lastChange=strftime("%Y-%m-%d %H:%M:%S")
    let l:add ="///++diulin  " . b:lastChange
    let l:drop="--diulin  " . b:lastChange
    let l:out= l:add . " {{{\n" . @" . l:add . " }}}\n///" . l:drop. " {{{\n". substitute(@","^\\|\\n","&///","g") . l:drop . " }}}\n"
    return l:out
  endfunction
endif 


" Add mappings, unless the user didn't want this.
if !exists("no_plugin_maps") && !exists("no_cos_maps")


	if !hasmapto('<Plug>cosAuthorChange')
		vmap <buffer> <LocalLeader>aa <Plug>cosAuthorChange
	endif
	vnoremap <buffer> <Plug>cosAuthorChange d:let @a=AuthorChange()<CR>"aP

endif
