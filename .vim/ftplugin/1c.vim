" Vim filetype plugin file
" Language:	1c
" Maintainer:	Diulin Andriy 
" Last Change:	2004 Sep 20

"TODO Search only in procedure or function. Or even better --- in any
"enclosing statement 


" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

let maplocalleader = ","

" set language preferences
"scriptencoding cp1251

let b:enoru = substitute(getline(search("^// .*1c:enoru=")),".*:enoru=\\(..\\).*","\\1","")
if b:enoru == ""
  let b:enoru = "ru"
endif


setlocal commentstring=\/\/%s
setlocal formatoptions=rown2

"set textwidth=70
setlocal dictionary=$HOME/vimruntime/ftplugin/1c.dict
"	.	scan the current buffer ('wrapscan' is ignored)
"	w	scan buffers from other windows
"	b	scan other loaded buffers that are in the buffer list
"	u	scan the unloaded buffers that are in the buffer list
"	U	scan the buffers that are not in the buffer list
"	k	scan the files given with the 'dictionary' option
"	k{dict}	scan the file {dict}.  Several k flags can be given,
setlocal cpt=.,w,b,u,t,i,k
setlocal tabstop=2
setlocal shiftwidth=2


"	Definitions for matchit
let b:match_words = '\<if\>:\<elsif\>:\<else\>:\<endif\>,'
		\ . '\<procedure\>:\<endprocedure\>,'
		\ . '\<function\>:\<endfunction\>,'
		\ . '\<while\>:\<continue\>:\<break\>:\<enddo\>,'
		\ . '\<for\>:\<continue\>:\<break\>:\<enddo\>,'
		\ . '\<Try\>:\<Except\>:\<EndTry\>,'
		\ . '\<Процедура\>:\<КонецПроцедуры\>,'
		\ . '\<Функция\>:\<КонецФункции\>,'
		\ . '\<Если\>:\<ИначеЕсли\>:\<Иначе\>:\<КонецЕсли\>,'
		\ . '\<Пока\>:\<КонецЦикла\>,'
		\ . '\<Для\>:\<КонецЦикла\>,'
		\ . '\<Попытка\>:\<Исключение\>:\<КонецПопытки\>'


setlocal foldmethod=syntax






"""nmap <buffer> <LocalLeader>fs :w $HOME/1c/notes/bak/=expand("%:r").".".strftime("%Y%m%d_%H%M%S").".".=expand("%:r")<CR><CR>
cabbrev ц w
cabbrev ив bd


"if !exists("*A2N")
"  finish
function! A2N(aaa)
  let ans=0
  let sss=a:aaa
  let lll=strlen(sss)
  let ttt=lll-1
  let md=0
  let mmm=1
  while ttt >= 0 
    let md = char2nr(strpart(sss,ttt,1))
    if md > 57
      let ans = ans + (md-55) * mmm
    else  
      let ans = ans + (md-48) * mmm
    endif
    let mmm = 36 * mmm
    let ttt = ttt-1
  endwhile
  return ans
endfunction
"endif 

if !exists("*N2A")
"  finish
function! N2A(nnn)
  let ans=""
  let char=""
  let nr=a:nnn
  let md=0
  while nr > 0
    let md = nr%36
    let char = (md > 9 ? nr2char(md+55) :nr2char(md+48))
    let ans=char . ans
    let nr = nr/36
  endwhile
  return ans
endfunction
endif 

if !exists("*NumRow")
function NumRow(start,end,step)
  let aa=a:end
  let cnt=a:start
  let stp=a:step
  let str=""
  while cnt<=aa
    let str= str . " " . cnt
    let cnt=cnt+stp
  endwhile
  return str
endfunction
endif 




" if !exists("*AutoProc")
function! AutoProc(num)
    let str = getline(a:num)
    let func0proc = substitute(str,"^\\s*\\([fpфп=]\\)\\? .*","\\1","")
    let str = substitute(str,"^\\s*\\([fpфп=]\\)\\s\\+","","")
    let str = substitute(str,"[();., ]\\+"," ","g")
    let name =  substitute(str,"^\\s*\\(.\\{-}\\)\\>.*","\\1","")
    let str =  substitute(str,"^\\(.\\{-}\\)\\>\\s*","","")
    let args =  substitute(str,"\\s*$","","")
    let args =  substitute(args,"\\s\\+",", ","g")
    if func0proc=="f" || func0proc=="ф"  || func0proc=="="
      if b:enoru == "ru"
        let st_begin="Функция"
        let st_end="КонецФункции"
      else  
        let st_begin="Function"
        let st_end="EndFunction"
      endif 
    else  
      if b:enoru == "ru"
        let st_begin="Процедура"
        let st_end="КонецПроцедуры"
      else  
        let st_begin="Procedure"
        let st_end="EndProcedure"
      endif 
    endif 
    let func_str =  st_begin . " " . name . "(" . args . ")"
    return func_str . "@@@" . st_end . "     // " . func_str
endfunction
 " endif 

if !exists("*AuthorChange")
  function! AuthorChange()
    let b:lastChange=strftime("%Y%m%d_%H%M%S")
    let l:add ="//++diulin  " . b:lastChange
    let l:drop="//--diulin  " . b:lastChange
    let l:out= l:add . "\n" . @" . l:add . "\n" . l:drop. "\n". substitute(@","^\\|\\n","&//","g") . l:drop . "\n"
    return l:out
  endfunction
endif 

" Add mappings, unless the user didn't want this.
if !exists("no_plugin_maps") && !exists("no_1c_maps")

  if !hasmapto('<Plug>1cCustFold')
    vmap <buffer> <LocalLeader>zf <Plug>1cCustFold
  endif
  vnoremap <buffer> <Plug>1cCustFold <Esc>`<$a //{{{<Esc>`>$a //}}}<Esc>`<zc
  if !hasmapto('<Plug>1cComment')
    vmap <buffer> <LocalLeader>// <Plug>1cComment
  endif
  vnoremap <buffer> <Plug>1cComment  <Esc>:let b:a=@/<CR>gv:s/^/\/\//<CR>:let @/=b:a<CR>
  if !hasmapto('<Plug>1cUncomment')
    vmap <buffer> <LocalLeader>?? <Plug>1cUncomment
  endif
  vnoremap <buffer> <Plug>1cUncomment  <Esc>:let b:a=@/<CR>gv:s/^\(\s*\)\/\//\1/<CR>:let @/=b:a<CR>
  " convert number under cursor from decimal into 36-mal
  if !hasmapto('<Plug>1cTo36')
    nmap <buffer> <LocalLeader>aj <Plug>1cTo36
  endif
  nnoremap <buffer> <Plug>1cTo36  yiw:let @*=A2N(@")<CR>:echo @*<CR>
  " convert number under cursor from 36-mal into decimal
  if !hasmapto('<Plug>1cFrom36')
    nmap <buffer> <LocalLeader>ah <Plug>1cFrom36
  endif
  nnoremap <buffer> <Plug>1cFrom36  yiw:let @*=N2A(@")<CR>:echo @*<CR>
  if !hasmapto('<Plug>1cAutoProc')
    nmap <buffer> <LocalLeader>ap <Plug>1cAutoProc
  endif
  nnoremap <buffer> <Plug>1cAutoProc :call append(".", AutoProc(line(".")))<CR>:d <CR>:s/@@@/\r/<CR>zoO<Tab><Esc>

  if !hasmapto('<Plug>1cAddLoadFromFile')
    nmap <buffer> <LocalLeader>al <Plug>1cAddLoadFromFile
  endif
  nnoremap <buffer> <Plug>1cAddLoadFromFile ggO#loadfromfile "<C-R>=expand("%:p")<CR>"<Esc>:s!^//!!<CR>
  
  if !hasmapto('<Plug>1cAddMessage')
    nmap <buffer> <LocalLeader>am <Plug>1cAddMessage
  endif
  nnoremap <buffer> <Plug>1cAddMessage oсообщить("");<Esc>

  if !hasmapto('<Plug>1cAuthorChange')
    vmap <buffer> <LocalLeader>aa <Plug>1cAuthorChange
  endif
  vnoremap <buffer> <Plug>1cAuthorChange d:let @a=AuthorChange()<CR>"aP

  if !hasmapto('<Plug>1cMessageFrenzyIn')
    nmap <buffer> <LocalLeader>mi <Plug>1cMessageFrenzyIn
  endif
  nnoremap <buffer> <Plug>1cMessageFrenzyIn :%s/\(\(\|.*\)\\|\(\/\/.*\)\\|\(\<Перем\>.*\)\)\@<!;/\="; message(\"(debug) at line ".line(".")."\");"/<CR><CR>
  
  if !hasmapto('<Plug>1cMessageFrenzyRegex')
    nmap <buffer> <LocalLeader>mr <Plug>1cMessageFrenzyRegex
  endif
  nnoremap <buffer> <Plug>1cMessageFrenzyRegex :g//exe 's/\(\(\|.*\)\\|\(\/\/.*\)\\|\(\<Перем\>.*\)\)\@<!;/\="; message(\"(debug ".line(".").")\");"/'<CR><CR>

  if !hasmapto('<Plug>1cMessageFrenzyOutAll')
    nmap <buffer> <LocalLeader>mO <Plug>1cMessageFrenzyOutAll
  endif
  nnoremap <buffer> <Plug>1cMessageFrenzyOutAll  :%s/\s*message("(debug.\{-};//g<CR>

  if !hasmapto('<Plug>1cMessageFrenzyDelAll')
    nmap <buffer> <LocalLeader>mD <Plug>1cMessageFrenzyDelAll
  endif
  nnoremap <buffer> <Plug>1cMessageFrenzyDelAll :g/^[	 /]*message("(debug[^;]*;$/d<CR>

  if !hasmapto('<Plug>1cVMessageFrenzyIn')
    vmap <buffer> <LocalLeader>mi <Plug>1cVMessageFrenzyIn
  endif
  vnoremap <buffer> <Plug>1cVMessageFrenzyIn :s/\(\(\|.*\)\\|\(\/\/.*\)\\|\(\<Перем\>.*\)\)\@<!;/\="; message(\"(debug) at line ".line(".")."\");"/<CR><CR>
  if !hasmapto('<Plug>1cVMessageFrenzyOut')
    vmap <buffer> <LocalLeader>mo <Plug>1cVMessageFrenzyOut
  endif
  vnoremap <buffer> <Plug>1cVMessageFrenzyOut :s/; message("\d\+");/;/g<CR>
  
  if !hasmapto('<Plug>1cVMessageVarValue')
    vmap <buffer> <LocalLeader>mv <Plug>1cVMessageVarValue
  endif
  vnoremap <buffer> <Plug>1cVMessageVarValue "byomessage("(debug <C-R>=line(".")<CR>) <C-R>b="+<C-R>b);<CR><Esc>

  if !hasmapto('<Plug>1cVMessageFrenzyOut')
    vmap <buffer> <LocalLeader>mo <Plug>1cVMessageFrenzyOut
  endif
  vnoremap <buffer> <Plug>1cVMessageFrenzyOut :s/; message("\d\+");/;/g<CR>
"Fixing wrong input kbd layout
  if !hasmapto('<Plug>1cSlipCommand')
    cmap <buffer> <LocalLeader>a, <Plug>1cSlipCommand
  endif
  cnoremap  <buffer> <Plug>1cSlipCommand d^i=FixLayout(@")<CR>
  if !hasmapto('<Plug>1cSlipInsert')
    imap <buffer> <LocalLeader>a, <Plug>1cSlipInsert
  endif
  inoremap  <buffer> <Plug>1cSlipInsert <Esc>lmsud`si=FixLayout(@")<CR>
  
  if !hasmapto('<Plug>1cNumRow')
    nmap <buffer> <LocalLeader>ar <Plug>1cNumRow
  endif
  nnoremap <buffer> <Plug>1cNumRow yy:call setline(".",NumRow("))<CR>

  if !hasmapto('<Plug>1cAutoIf')
    nmap <buffer> <LocalLeader>ai <Plug>1cAutoIf
  endif

 if !hasmapto('<Plug>1cAutoWhile')
   nmap <buffer> <LocalLeader>aw <Plug>1cAutoWhile
 endif
 nnoremap <buffer> <Plug>1cAutoWhile oПока () Цикл<CR>КонецЦикла;O  ;k^f(a
 
 if !hasmapto('<Plug>1cAutoFor')
   nmap <buffer> <LocalLeader>af <Plug>1cAutoFor
 endif
 " nnoremap <buffer> <Plug>1cAutoFor oПока () Цикл<CR>КонецЦикла;O  ;k^f(a
 nnoremap <buffer> <Plug>1cAutoFor oДля По  Цикл<CR>КонецЦикла;O  ;k^f i

 if !hasmapto('<Plug>1cMessage')
   nmap <buffer> <LocalLeader>mm <Plug>1cMessage
 endif
 nnoremap <buffer> <Plug>1cMessage omessage("(debug) ");<Esc>h

 if !hasmapto('<Plug>1cMessageExpression')
   vmap <buffer> <LocalLeader>me <Plug>1cMessageExpression
 endif
 vnoremap <buffer> <Plug>1cMessageExpression <Esc>:let b:tmp=@"<CR>gvyomessage("(debug) <C-R>"="+<C-R>");<Esc>
 
 if !hasmapto('<Plug>1cDebugExpression')
   vmap <buffer> <LocalLeader>md <Plug>1cDebugExpression
 endif
 vnoremap <buffer> <Plug>1cDebugExpression <Esc>:let b:tmp=@"<CR>gvyodebug("<C-R>"="+<C-R>");<Esc>

 if !hasmapto('<Plug>1cAutoIf')
   nmap <buffer> <LocalLeader>ai <Plug>1cAutoIf
 endif
 nnoremap <buffer> <Plug>1cAutoIf oЕсли () Тогда<CR>КонецЕсли;O<Tab>;k^f(a
 
endif


" nmap <buffer><LocalLeader>gh [(
" nmap <buffer><LocalLeader>gl ])
vmap <buffer><LocalLeader>vv <Esc>]%v%
nmap <buffer><LocalLeader>vv [%v%
